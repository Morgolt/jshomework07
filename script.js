/*1. Опишіть своїми словами як працює метод forEach.

Метод forEach дозволяє послідовно перебрати всі елементи масиву.


2. Як очистити масив?

1) Щоб очистити масив від елементів, потрібно встановити його довжину на нуль. 
Ця операція видаляє всі елементи масиву, і навіть їх значення.

massiv.length = 0;

2) Через присвоєння нового порожнього масиву

massiv = [];


3. Як можна перевірити, що та чи інша змінна є масивом?

Для того, щоб перевірити, чи є змінна масивом, можна скористатися вбудованим методом Array.isArray().

const massiv = [1, 2, 3];
console.log(Array.isArray(numbers)); // => true

*/

const arrayFirst = ['hello', 'world', 23, '23', null];
const filterBy = (arrayFirst, type) => {
  return arrayFirst.filter((element) => typeof element !== type);
};
console.log(filterBy(arrayFirst, 'object'));
console.log(filterBy(arrayFirst, 'number'));
console.log(filterBy(arrayFirst, 'string'));
